
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "dw_std.h"

t main(void)
{
  int m=1; 
  int n=7;
  double X[7];
  double tau[1];
  
  int lwork, info;
  double *work, opt_size;
  
  int i;

  // Randomly generate 1x7 matrix X
  for (i=6; i >= 0; i--) X[i]=(double)rand()/RAND_MAX;

  // Make size inquiry
  lwork=-1;
  dgeqrf(&m,&n,X,&m,tau,&opt_size,&lwork,&info);

  // Attempt QR decomposition
  work=(double*)dw_malloc((lwork=(int)opt_size)*sizeof(double));
  dgeqrf(&m,&n,X,&m,tau,work,&lwork,&info);

  // Is problem?
  if (info)
    {
      printf("Unexpected error: info= %d  lwork= %d  m = %d  n = %d\n",info,lwork,m,n);

      // work around
      dw_free(work);
      work=(double*)dw_malloc((lwork=(m > n) ? m*m : n*n)*64*sizeof(double));
      dgeqrf(&m,&n,X,&m,tau,work,&lwork,&info);
      printf("info = %d  work[0]= %d\n",info,(int)work[0]);
    }
  else
    printf("Successful\n");
}
