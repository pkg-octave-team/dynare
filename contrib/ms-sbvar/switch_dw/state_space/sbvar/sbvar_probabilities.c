/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#include "switch.h"
#include "switchio.h"
#include "VARio.h"
#include "dw_parse_cmd.h"
#include "dw_ascii.h"
#include "dw_error.h"
#include "dw_sbvar_command_line.h"
#include "dw_std.h"

#include <stdlib.h>
#include <string.h>

/*
   Attempt to set up model from command line.  Command line options are the 
   following

   -filtered
      If this argument is set, then the filtered probabilities are computed.  The 
      default value is off.

   -smoothed
      If this argument is set, then the smoothed probabilities are computed.  The
      default value is on if neither -filtered or -real_time_smoothed are set.

   -real_time_smoothed
      If this augument is set, then the smoothed probabilities based on time t
      information are computed for 0 <= t <= nobs.  The default value is off.

*/
void dw_sbvar_probabilities_command_line(int nargs, char **args)
{
  char *out_filename;
  TStateModel *model;
  TVector *probabilities, **rt_probabilities;
  int s, t, tau, smoothed=0, filtered=0, real_time=0;
  FILE *f_out;
  TVARCommandLine *cmd;

  // Setup model and initial parameters
  if (!(cmd=CreateTStateModel_VARCommandLine(nargs,args,(TVARCommandLine*)NULL)))
    dw_Error(MEM_ERR);
  else
    if (!cmd->model)
      dw_UserError("Unable to setup SBVAR from command line parameters");
    else
      if (!cmd->parameters_filename_actual)
	dw_UserError("Unable to initialize parameters from command line parameters");
      else
	{
	  model=cmd->model;

	  // filtered probabilities
	  if (dw_FindArgument_String(nargs,args,"filtered") != -1)
	    filtered=1;

	  // real-time smoothed probabilities
	  if (dw_FindArgument_String(nargs,args,"real_time_smoothed") != -1)
	    real_time=1;

	  // smoothed probabilities
	  if ((!filtered && !real_time) || (dw_FindArgument_String(nargs,args,"smoothed") != -1))
	    smoothed=1;

	  // Output tag
	  OutTag_VARCommandLine(cmd);

	  if (smoothed)
	    {
	      out_filename=CreateFilenameFromTag("%ssmoothed_%s.out",cmd->out_tag,cmd->out_directory);
	      if ((f_out=dw_CreateTextFile(out_filename)))
		{
		  probabilities=dw_CreateArray_vector(model->sv->nstates);
		  for (s=model->sv->nstates-1; s >= 0; s--)
		    probabilities[s]=ProbabilitiesState((TVector)NULL,s,model);
		  for (t=0; t <= model->nobs; t++)
		    {
		      for (s=0; s < model->sv->nstates; s++)
			fprintf(f_out,"%lf ",ElementV(probabilities[s],t));
		      fprintf(f_out,"\n");
		    }
		  dw_FreeArray(probabilities);
		  fclose(f_out);
		}
	      dw_free(out_filename);	      
	    }

	  if (filtered)
	    {
	      out_filename=CreateFilenameFromTag("%sfiltered_%s.out",cmd->out_tag,cmd->out_directory);
	      if ((f_out=dw_CreateTextFile(out_filename)))
		{
		  probabilities=dw_CreateArray_vector(model->sv->nstates);
		  for (s=model->sv->nstates-1; s >= 0; s--)
		    {
		      probabilities[s]=CreateVector(model->nobs);
		      for (t=model->nobs; t >= 0; t--)
			ElementV(probabilities[s],t)=ProbabilityStateConditionalCurrent(s,t,model);
		    }
		  for (t=0; t <= model->nobs; t++)
		    {
		      for (s=0; s < model->sv->nstates; s++)
			fprintf(f_out,"%lf ",ElementV(probabilities[s],t));
		      fprintf(f_out,"\n");
		    }
		  dw_FreeArray(probabilities);
		  fclose(f_out);
		}
	      dw_free(out_filename);
	    }

	  if (real_time)
	    {
	      out_filename=CreateFilenameFromTag("%sreal_time_smoothed_%s.out",cmd->out_tag,cmd->out_directory);
	      if ((f_out=dw_CreateTextFile(out_filename)))
		{
		  //rt_probabilities=(TVector**)dw_CreateArray_pointer(model->nobs+1,&dw_free);
		  rt_probabilities=(TVector**)dw_CreateArray_array(model->nobs+1);
		  for (tau=model->nobs; tau >= 0; tau--)
		    rt_probabilities[tau]=RealTimeSmoothedProbabilities((TVector*)NULL,tau,model);
		  for (t=0; t <= model->nobs; t++)
		    {
		      for (tau=0; tau < t; tau++)
			for (s=0; s < model->sv->nstates; s++)
			  fprintf(f_out,", ");
		      for ( ; tau <= model->nobs; tau++)
			for (s=0; s < model->sv->nstates; s++)
			  fprintf(f_out,"%lf, ",ElementV(rt_probabilities[tau][t],s));
		      fprintf(f_out,"\n");
		    }
		  dw_FreeArray(rt_probabilities);
		  fclose(f_out);
		}
	      dw_free(out_filename);
	    }
	}

  Free_VARCommandLine(cmd);
}
