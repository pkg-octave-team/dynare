
#include "dw_matrix.h"
#include "VARbase.h"

void impulse_response(TMatrix ir, int *S, TMatrix A0_Xi, TMatrix* B);
void GetIRMatrices(int h, TMatrix *A0_Xi, TMatrix *B, T_VAR_Parameters *p);

TMatrix IRHistogramToPercentile(TMatrix F, int horizon, PRECISION percentile, TMatrixHistogram *histogram, TStateModel *model);
TMatrixHistogram* impulse_response_percentile_ergodic(int draws, FILE *posterior_file, int thin, int ergodic, int horizon, TStateModel *model, int flag);
TMatrixHistogram* impulse_response_percentile_regime(FILE *posterior_file, int thin, int s, int horizon, TStateModel *model, int flag);
