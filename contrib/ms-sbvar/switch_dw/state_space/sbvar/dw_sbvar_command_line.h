/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __command_line_VAR
#define __command_line_VAR

#include "switch.h"
#include <stdio.h>

void dw_sbvar_simulate_command_line(int nargs, char **args);
void dw_sbvar_estimate_command_line(int nargs, char **args);
void dw_sbvar_probabilities_command_line(int nargs, char **args);
void dw_sbvar_mdd_command_line(int nargs, char **args);
void dw_sbvar_forecast_command_line(int nargs, char **args);
void dw_sbvar_impulse_response_command_line(int nargs, char **args);
void dw_sbvar_variance_decomposition_command_line(int nargs, char **args);
void dw_sbvar_test_command_line(int nargs, char **args);

#endif
