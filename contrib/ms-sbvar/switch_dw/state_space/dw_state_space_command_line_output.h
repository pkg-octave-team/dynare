/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __command_line_state_space_output__
#define __command_line_state_space_output__

#ifdef __cplusplus
extern "C" {
#endif

#include "dw_switch.h"

void dw_command_line_statespace_output(int nargs, char** args, TStateModel *model);

void dw_state_space_historical_decomposition_command_line(int nargs, char **args, TStateModel *model);
void dw_state_space_smoothed_shocks_command_line(int nargs, char **args, TStateModel *model);
void dw_state_space_smoothed_states_command_line(int nargs, char **args, TStateModel *model);
void dw_state_space_impulse_response_command_line(int n_args, char **args, TStateModel *model);
void dw_state_space_probabilities_command_line(int n_args, char **args, TStateModel *model);

#ifdef __cplusplus
}
#endif  
  
#endif
