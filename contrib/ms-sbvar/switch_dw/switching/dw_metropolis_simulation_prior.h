/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __METROPOLIS_SIMULATION_PRIOR__
#define __METROPOLIS_SIMULATION_PRIOR__

void dw_simulate_prior_command_line(int n_args, char **args, TStateModel *model, TVector start_value, int generator_init);
int SetUpMetropolisSimulationPrior(int n_args, char **args, char *tag, TStateModel *model, int default_period);
int SimulatePrior(int burn_in, int draws, int thin, int period, TStateModel *model, FILE *f_out);

#endif
