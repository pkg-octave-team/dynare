/*
 * Copyright (C) 1996-2012 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __dw_metropolis_block__
#define __dw_metropolis_block__

#include "dw_matrix.h"
#include "dw_switch.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*******************************************************************************/
/**************************** TMetropolis Structure ****************************/
/*******************************************************************************/
typedef struct TMetropolis_block_tag
{
  //=== Metropolis parameters ===
  int n_parameters;
  int n_blocks;
  int *block_dims;
  int *block_offsets;
  TVector scale;
  TVector *direction;

  //=== Simulation status fields ===
  int *jumps;
  int *draws;

  //=== Simulation functions ===
  void *structure;
  void (*pGetParameters)(void*, PRECISION*);
  void (*pPutParameters)(void*, PRECISION*);
  PRECISION (*pDensity)(void*);

  //=== Other parameters ===
  int n_other_parameters;
  void (*pGetOtherParameters)(void*, PRECISION*);
  void (*pPutOtherParameters)(void*, PRECISION*);
  PRECISION (*pDrawGibbs)(PRECISION, void*);

} TMetropolis_block;

TMetropolis_block* Create_metropolis_block(int n_parameters, void (*Get)(void*, PRECISION*), void (*Put)(void*, PRECISION*),PRECISION (*Density)(void*),PRECISION (*Gibbs)(PRECISION, void*));
void Free_metropolis_block(TMetropolis_block *metropolis);

void Setup_metropolis(TMetropolis_block *metropolis, TVector scale, TVector *directions);
void Setup_metropolis_diagonal(TMetropolis_block *metropolis);
void Setup_metropolis_hessian(TMetropolis_block *metropolis, TMatrix hessian);
void Setup_metropolis_variance(TMetropolis_block *metropolis, TMatrix variance);

void Setup_metropolis_blocks(TMetropolis_block *metropolis, int n_blocks, int *dims);
void Setup_metropolis_full(TMetropolis_block *metropolis);
void Setup_metropolis_single(TMetropolis_block *metropolis);

void ResetCounts_metropolis_block(TMetropolis_block *metropolis);

PRECISION draw_metropolis_in_gibbs(PRECISION logdensity, TMetropolis_block *metropolis);
PRECISION metropolis_draw_all_blocks(PRECISION logdensity, TMetropolis_block *metropolis);
PRECISION metropolis_draw_single_block(PRECISION logdensity, int block, TMetropolis_block *metropolis);

void Calibrate_metropolis_theta(TStateModel *model, PRECISION center, int period, int max_period, int verbose);
int Calibrate_metropolis_theta_single_direction(int idx, TStateModel *model, PRECISION center, int period, int max_period, int verbose);
int Calibrate_metropolis_theta_two_pass(TStateModel *model, PRECISION center_s,	int period_s, int max_period_s, PRECISION center_a, int period_a, int max_period_a, int verbose);


/*******************************************************************************/
/****************************** Adaptive Scaling *******************************/
/*******************************************************************************/
typedef struct
{
  int period;
  int begin_draws;
  int end_draws;
  int begin_jumps;
  PRECISION previous_ratio;
  PRECISION scale;
  PRECISION best_scale;
  PRECISION low_scale;
  PRECISION low_jump_ratio;
  PRECISION high_scale;
  PRECISION high_jump_ratio;

  PRECISION mid;        
  PRECISION log_mid;    
  PRECISION lower_bound; 
  PRECISION upper_bound;

} TAdaptive;
TAdaptive* CreateAdaptive(PRECISION mid);
void SetAdaptiveCenter(PRECISION mid, TAdaptive *adaptive);
void InitializeAdaptive(int period, int block, TAdaptive *adaptive, TMetropolis_block *metropolis);
PRECISION RecomputeScale(int end_jumps, int end_draws, TAdaptive *adaptive);
void PrintJumpingInfo(int b, TAdaptive *adaptive, TMetropolis_block *metropolis);
void AdaptiveScale_metropolis_theta(TStateModel *model, PRECISION mid, int period, int max_period, int verbose);
void AdaptiveScale_metropolis_theta_single_block(int block, TStateModel *model, PRECISION mid, int period, int max_period, int verbose);
  
#ifdef __cplusplus
}
#endif  
  
#endif
