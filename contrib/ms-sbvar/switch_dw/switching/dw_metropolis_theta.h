/*
 * Copyright (C) 1996-2011 Daniel Waggoner and Tao Zha
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __dw_metropolis_theta__
#define __dw_metropolis_theta__

#include "dw_matrix.h"
#include "dw_switch.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*******************************************************************************/
/**************************** TMetropolis Structure ****************************/
/*******************************************************************************/
typedef struct TMetropolis_theta_tag
{
  //=== Metropolis parameters ===
  int n_parameters;
  int n_blocks;
  int *block_dims;
  int *block_offsets;
  TVector scale;
  TVector *direction;

  //=== Simulation status fields ===
  int *jumps;
  int *draws;

} TMetropolis_theta;

TMetropolis_theta* Create_metropolis_theta(int n_parameters);
void Free_metropolis_theta(TMetropolis_theta *metropolis);

void Setup_metropolis_theta(TMetropolis_theta *metropolis, TVector scale, TVector *directions);
void Setup_metropolis_theta_diagonal(TMetropolis_theta *metropolis);
void Setup_metropolis_theta_hessian(TMetropolis_theta *metropolis, TMatrix hessian);
void Setup_metropolis_theta_variance(TMetropolis_theta *metropolis, TMatrix hessian);

void Setup_metropolis_theta_blocks(TMetropolis_theta *metropolis, int n_blocks, int *dims);
void Setup_metropolis_theta_blocks_full(TMetropolis_theta *metropolis);
void Setup_metropolis_theta_blocks_single(TMetropolis_theta *metropolis);

void ResetCounts_metropolis_theta(TMetropolis_theta *metropolis);

void Draw_metropolis_theta(TStateModel *model);
void Draw_metropolis_theta_single_block(int block, TStateModel *model);

void AdaptiveScale_metropolis_theta(TStateModel *model, PRECISION mid, int period, int max_period, int verbose);
void AdaptiveScale_metropolis_theta_single_block(int block, TStateModel *model, PRECISION mid, int period, int max_period, int verbose);

void Calibrate_metropolis_theta(TStateModel *model, PRECISION center, int period, int max_period, int verbose);
int Calibrate_metropolis_theta_single_direction(int idx, TStateModel *model, PRECISION center, int period, int max_period, int verbose);
int Calibrate_metropolis_theta_two_pass(TStateModel *model, PRECISION center_s,	int period_s, int max_period_s, 
					PRECISION center_a, int period_a, int max_period_a, int verbose);


/*******************************************************************************/
/****************************** Adaptive Scaling *******************************/
/*******************************************************************************/
//=== Adaptive scaling
typedef struct
{
  int period;
  int begin_draws;
  int end_draws;
  int begin_jumps;
  PRECISION previous_ratio;
  PRECISION scale;
  PRECISION best_scale;
  PRECISION low_scale;
  PRECISION low_jump_ratio;
  PRECISION high_scale;
  PRECISION high_jump_ratio;

  PRECISION mid;        
  PRECISION log_mid;    
  PRECISION lower_bound; 
  PRECISION upper_bound;

} TAdaptive;
TAdaptive* CreateAdaptive(PRECISION mid);
void SetAdaptiveCenter(PRECISION mid, TAdaptive *adaptive);
void InitializeAdaptive(int period, int block, TAdaptive *adaptive, TMetropolis_theta *metropolis);
PRECISION RecomputeScale(int end_jumps, int end_draws, TAdaptive *adaptive);
void PrintJumpingInfo(int b, TAdaptive *adaptive, TMetropolis_theta *metropolis);
  
#ifdef __cplusplus
}
#endif  
  
#endif
